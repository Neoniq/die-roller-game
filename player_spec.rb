require_relative 'player'

describe Player do

  before do
    @initial_health = 150
    @player = Player.new("larry", @initial_health)
  end

  context "sets up a player with an initial health of 150" do
    it "is strong" do
      @player.should be_strong
    end

    context "with a health of 100 or less" do
      before do
        @player = Player.new("larry", 100)
      end

      it "is wimpy" do
        @player.should_not be_strong
      end
    end
  end
end